# CMake generated Testfile for 
# Source directory: /home/haraami/catkin_workspace/src/serial/tests
# Build directory: /home/haraami/catkin_workspace/build/serial/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(_ctest_serial_gtest_serial-test "/home/haraami/catkin_workspace/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/indigo/share/catkin/cmake/test/run_tests.py" "/home/haraami/catkin_workspace/build/test_results/serial/gtest-serial-test.xml" "--return-code" "/home/haraami/catkin_workspace/devel/lib/serial/serial-test --gtest_output=xml:/home/haraami/catkin_workspace/build/test_results/serial/gtest-serial-test.xml")
ADD_TEST(_ctest_serial_gtest_serial-test-timer "/home/haraami/catkin_workspace/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/indigo/share/catkin/cmake/test/run_tests.py" "/home/haraami/catkin_workspace/build/test_results/serial/gtest-serial-test-timer.xml" "--return-code" "/home/haraami/catkin_workspace/devel/lib/serial/serial-test-timer --gtest_output=xml:/home/haraami/catkin_workspace/build/test_results/serial/gtest-serial-test-timer.xml")
