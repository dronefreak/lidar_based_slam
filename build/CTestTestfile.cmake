# CMake generated Testfile for 
# Source directory: /home/haraami/catkin_workspace/src
# Build directory: /home/haraami/catkin_workspace/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(mavros/libmavconn)
SUBDIRS(serial)
SUBDIRS(mavros/mavros_msgs)
SUBDIRS(beginner_tutorials)
SUBDIRS(lightware_sf40c_ros_driver)
SUBDIRS(mavros/mavros)
SUBDIRS(mavros_testing)
SUBDIRS(mavros/test_mavros)
