# CMake generated Testfile for 
# Source directory: /home/haraami/catkin_workspace/src/mavros/libmavconn
# Build directory: /home/haraami/catkin_workspace/build/mavros/libmavconn
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(_ctest_libmavconn_gtest_mavconn-test "/home/haraami/catkin_workspace/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/indigo/share/catkin/cmake/test/run_tests.py" "/home/haraami/catkin_workspace/build/test_results/libmavconn/gtest-mavconn-test.xml" "--return-code" "/home/haraami/catkin_workspace/devel/lib/libmavconn/mavconn-test --gtest_output=xml:/home/haraami/catkin_workspace/build/test_results/libmavconn/gtest-mavconn-test.xml")
