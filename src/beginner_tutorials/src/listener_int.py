#!/usr/bin/env python

from your_package.msg import Foo

def mytopic_callback(msg):
    print "Here are some integers:", str(msg.some_integers)
    print "Here are some floats:", str(msg.some_floats)

mysub = rospy.Subscriber('mytopic', Foo, mytopic_callback)