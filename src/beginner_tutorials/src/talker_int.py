#!/usr/bin/env python

import rospy
import std_msgs

def talker_int():
	pub = rospy.Publisher('chatter', std_msgs.msg.Int32, queue_size=10)
	rospy.init_node('talker_int', anonymous=True)
	rate = rospy.Rate(10)
	while not rospy.is_shutdown():
		hello_str=10
		pub.publish(hello_str)
		rate.sleep()

if __name__ == '__main__':
	try:
		talker_int()
	except rospy.ROSInterruptException:
		pass
